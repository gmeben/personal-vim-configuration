# Personal Vim Configuration

This contains my personal configuration files for vim, gvim, and MacVim.

* **/vimfiles** - Directory for files used by Vim. Curently, this contains files for a color scheme and SCSS support.
* **.vimrc** - Configuration file
* **_gvimrc** - Windows GUI configuration file
* **_vimrc** - Windows configuration file

